class Month
  MONTHS = {
    1 => 31,
    2 => 28,
    3 => 31,
    4 => 30,
    5 => 31,
    6 => 30,
    7 => 31,
    8 => 31,
    9 => 30,
    10=> 31,
    11=> 30,
    12=> 31
  }

  DAYS = {
    0 => 'Saturday',
    1 => 'Sunday',
    2 => 'Monday',
    3 => 'Tuesday',
    4 => 'Wednesday',
    5 => 'Thursday',
    6 => 'Friday'
  }

  attr_reader :month, :year_offset

  def initialize (month, year_offset=0)
    @month = month
    @year_offset = year_offset
  end

  def ends_on
    months = MONTHS.select {|k,v| k <= month}

    days = months.values.sum

    day = (days + @year_offset) % 7

    DAYS[day]
  end
end