require './month.rb'

RSpec.describe Month, ".even?" do
  context 'without year offset' do
    it 'gives the month as not ending on Saturday' do
      month = Month.new 3

      expect(month.ends_on).to_not include 'Saturday'
    end

    it 'gives month as ending on Saturday' do
      month = Month.new 9

      expect(month.ends_on).to include 'Saturday'
    end
  end

  context 'with year offset' do
    it 'gives the month as ending on Saturday' do
      month = Month.new 3, 1

      expect(month.ends_on).to include 'Saturday'
    end

    it 'gives month as not ending on Saturday' do
      month = Month.new 9, 1

      expect(month.ends_on).to_not include 'Saturday'
    end

  end
end